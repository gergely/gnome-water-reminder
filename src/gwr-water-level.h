#ifndef __GWR_WATER_LEVEL_H__
# define __GWR_WATER_LEVEL_H__

# include <gtk/gtk.h>

G_BEGIN_DECLS

typedef enum {
    GWR_WATER_LEVEL_STYLE_GLASS,
    GWR_WATER_LEVEL_STYLE_BOTTLE
} GwrWaterLevelStyle;

# define GWR_TYPE_WATER_LEVEL (gwr_water_level_get_type())
G_DECLARE_FINAL_TYPE(GwrWaterLevel, gwr_water_level, GWR, WATER_LEVEL, GtkWidget)

void gwr_water_level_set_level(GwrWaterLevel *water_level,
                               gfloat         level);
gfloat gwr_water_level_get_level(GwrWaterLevel *water_level);
void gwr_water_level_set_style(GwrWaterLevel      *water_level,
                               GwrWaterLevelStyle  style);
GwrWaterLevelStyle gwr_water_level_get_style(GwrWaterLevel *water_level);

G_END_DECLS

#endif  /* __GWR_WATER_LEVEL_H__ */
