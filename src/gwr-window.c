/* gwr-window.c
 *
 * Copyright 2018 Gergely Polonkai
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gwr-config.h"

#include <glib/gi18n-lib.h>
#include "gwr-window.h"
#include "gwr-water-level.h"

struct _GwrWindow
{
    GtkApplicationWindow  parent_instance;

    /* Template widgets */
    GtkHeaderBar        *header_bar;
    GwrWaterLevel       *level;
    GwrWaterLevel       *remaining;

    GtkLabel            *remaining_value;
    GtkLabel            *current_value;
};

G_DEFINE_TYPE (GwrWindow, gwr_window, GTK_TYPE_APPLICATION_WINDOW)

static void gwr_window_add_action(GSimpleAction *action,
                                  GVariant      *parameter,
                                  gpointer       user_data);

static GActionEntry win_entries[] = {
    { "add", gwr_window_add_action, NULL, NULL, NULL },
};

static void
gwr_window_class_init (GwrWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/eu/polonkai/gergely/GnomeWaterReminder/gwr-window.ui");
  gtk_widget_class_bind_template_child (widget_class, GwrWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, GwrWindow, level);
  gtk_widget_class_bind_template_child (widget_class, GwrWindow, remaining);
  gtk_widget_class_bind_template_child (widget_class, GwrWindow, current_value);
  gtk_widget_class_bind_template_child (widget_class, GwrWindow, remaining_value);
}

static void
level_changed(GwrWaterLevel *level,
              GParamSpec    *pspec,
              GwrWindow     *self)
{
    gchar *text;
    GtkLabel *label = (level == self->level) ? self->current_value : self->remaining_value;

    g_debug("level changed");

    text = g_strdup_printf(_("Value: %.0f%%"), gwr_water_level_get_level (level) * 100);
    gtk_label_set_text(label, text);
    g_free(text);
}

static void
gwr_window_init (GwrWindow *self)
{
    g_type_ensure(GWR_TYPE_WATER_LEVEL);

    gtk_widget_init_template (GTK_WIDGET (self));

    g_action_map_add_action_entries(G_ACTION_MAP(self), win_entries, G_N_ELEMENTS(win_entries), self);

    g_signal_connect(self->level, "notify::level", G_CALLBACK(level_changed), self);
    g_signal_connect(self->remaining, "notify::level", G_CALLBACK(level_changed), self);

    level_changed(self->level, NULL, self);
    level_changed(self->remaining, NULL, self);
}

void
gwr_window_add_water(GwrWindow *self)
{
    gwr_water_level_set_level (self->remaining, gwr_water_level_get_level (self->remaining) - 0.1);
    gwr_water_level_set_level (self->level, gwr_water_level_get_level (self->level) + 0.1);
}

static void
 gwr_window_add_action(GSimpleAction *action,
                       GVariant      *parameter,
                       gpointer       user_data)
{
    gwr_window_add_water(GWR_WINDOW(user_data));
}
