/* main.c
 *
 * Copyright 2018 Gergely Polonkai
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <libnotify/notify.h>

#include "gwr-config.h"
#include "gwr-window.h"

static void
add_some(NotifyNotification *notification,
         char               *action,
         gpointer            user_data)
{
    GwrWindow *window = GWR_WINDOW(user_data);
    GError *error = NULL;

    if (!notify_notification_close(notification, &error)) {
        g_warning("Could not close notification: %s", error->message);

        g_clear_error(&error);
    }

    gwr_window_add_water(window);
}

static gboolean
show_notification(gpointer user_data)
{
    NotifyNotification *notification;
    GwrWindow *window = GWR_WINDOW(user_data);
    GError *error = NULL;

    notification = notify_notification_new(_("Remember to drink water!"), NULL, NULL);
    notify_notification_set_timeout(notification, 5000);
    notify_notification_add_action(notification, "add", _("I just drank"), add_some, g_object_ref(window), g_object_unref);

    if (!notify_notification_show(notification, &error)) {
        g_warning("Could not present notification: %s", error->message);
        g_clear_error(&error);
    }
}

static void
on_activate (GtkApplication *app)
{
	GtkWindow *window;

	g_assert (GTK_IS_APPLICATION (app));

	/* Get the current window or create one if necessary. */
	window = gtk_application_get_active_window (app);

    if (window == NULL) {
        window = g_object_new (GWR_TYPE_WINDOW,
                               "application", app,
                               NULL);
    }

	/* Ask the window manager/compositor to present the window. */
	gtk_window_present (window);

    g_timeout_add_seconds(3600, show_notification, window);
}

int
main (int   argc,
      char *argv[])
{
    gint ret;

	g_autoptr(GtkApplication) app = NULL;

	/* Set up gettext translations */
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	app = gtk_application_new ("eu.polonkai.gergely.GnomeWaterReminder", G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

    notify_init(_("Water Reminder"));

    ret = g_application_run (G_APPLICATION (app), argc, argv);

    notify_uninit();

    return ret;
}
